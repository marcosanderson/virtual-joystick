﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SigmaMadIdea.VirtualJoystick;

public class BlockMovement : MonoBehaviour {

    public JoystickController joystick;

    public float speed;


    RectTransform rectTransform;
    Image myImage;

	// Use this for initialization
	void Start () {
        rectTransform = GetComponent<RectTransform>();
        myImage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!rectTransform) return;

        rectTransform.anchoredPosition += speed * joystick.AnalogDirection();

        myImage.color = Color.white;

        if (joystick.IsUpCommandClicked())
            myImage.color = Color.cyan;

        if (joystick.IsDownCommandClicked())
            myImage.color = Color.blue;

        if (joystick.IsLeftCommandClicked())
            myImage.color = Color.red;

        if (joystick.IsRightCommandClicked())
            myImage.color = Color.green;
        
    }
}
