﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace SigmaMadIdea.VirtualJoystick
{
    internal class AnalogStick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        private JoystickController JoystickController { get; set; }

        private RectTransform InnerStick;

        private Vector2 inputDirection;

        private void Awake()
        {
            JoystickController = GameObject.FindGameObjectWithTag("JoystickController").GetComponent<JoystickController>();

            if (transform.childCount > 0)
                InnerStick = transform.GetChild(0).GetComponent<RectTransform>();
            else
                Debug.Log("This will not work properly. Please set an inner stick for this component.");
        }

        public void OnDrag(PointerEventData ped)
        {
            if (InnerStick == null) return;

            Vector2 pos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), ped.position, ped.pressEventCamera, out pos))
            {
                pos.x = pos.x / GetComponent<RectTransform>().sizeDelta.x;
                pos.y = pos.y / GetComponent<RectTransform>().sizeDelta.y;

                inputDirection = new Vector2(pos.x * 2 - 1, pos.y);
                inputDirection = (inputDirection.magnitude > 1) ? inputDirection.normalized : inputDirection;

                InnerStick.anchoredPosition = inputDirection * JoystickController.AnalogStickRadius;
            }
        }

        public void OnPointerDown(PointerEventData ped)
        {
            OnDrag(ped);
        }

        public void OnPointerUp(PointerEventData ped)
        {
            inputDirection = Vector2.zero;
            InnerStick.anchoredPosition = Vector2.zero;
        }

        public Vector2 InputDirection()
        {
            return inputDirection;
        }
        public float Horizontal()
        {
            return inputDirection.x;
        }
        public float Vertical()
        {
            return inputDirection.y;
        }
    }
}