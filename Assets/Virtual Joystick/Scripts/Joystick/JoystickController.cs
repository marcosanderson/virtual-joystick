﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


namespace SigmaMadIdea.VirtualJoystick
{

    public class JoystickController : MonoBehaviour
    {
        /// <summary>
        /// The bellow methods are related to the buttons commands (X, A, B, Y - or X, O, Δ, □)
        /// </summary>
        [SerializeField]
        private ButtonController UpCommandButton;
        [SerializeField]
        private ButtonController RightCommandButton;
        [SerializeField]
        private ButtonController DownCommandButton;
        [SerializeField]
        private ButtonController LeftCommandButton;

        [SerializeField]
        private AnalogStick AnalogStick;

        /// This could be calculated using the radius of a circle collider 2d
        [SerializeField]
        internal float ButtonRadius;
        /// This could be calculated using the radius of a circle collider 2d
        [SerializeField]
        internal float AnalogStickRadius;

        [SerializeField]
        internal Color NormalColor, HoverColor;


        public bool IsUpCommandClicked()
        {
            return (UpCommandButton) ? UpCommandButton.Clicked : false;
        }
        public bool IsDownCommandClicked()
        {
            return (DownCommandButton) ? DownCommandButton.Clicked : false;
        }
        public bool IsRightCommandClicked()
        {
            return (RightCommandButton) ? RightCommandButton.Clicked : false;
        }
        public bool IsLeftCommandClicked()
        {
            return (LeftCommandButton) ? LeftCommandButton.Clicked : false;
        }

        public float Horizontal()
        {
            return AnalogStick.Horizontal();
        }
        public float Vertical()
        {
            return AnalogStick.Vertical();
        }
        public Vector2 AnalogDirection()
        {
            return AnalogStick.InputDirection();
        }
    }
}