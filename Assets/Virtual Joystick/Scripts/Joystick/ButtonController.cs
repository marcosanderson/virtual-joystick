﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SigmaMadIdea.VirtualJoystick
{
    [RequireComponent(typeof(RectTransform))]
    internal class ButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private JoystickController JoystickController { get; set; }

        internal bool Clicked { get; private set; }

        private void Awake()
        {
            JoystickController = GameObject.FindGameObjectWithTag("JoystickController").GetComponent<JoystickController>();
        }

        public void OnPointerDown(PointerEventData ped)
        {
            Vector2 pos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), ped.pressPosition, ped.pressEventCamera, out pos))
            {
                Clicked = IsCloseCenter(pos, JoystickController.ButtonRadius);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Clicked = false;
        }
        public void OnPointerEnter(PointerEventData ped)
        {
            HoverButton(GetComponent<Image>(), true);
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            HoverButton(GetComponent<Image>(), false);
        }


        private bool IsCloseCenter(Vector2 point, float radius)
        {
            return (Vector2.Distance(Vector2.zero, point) < radius);
        }
        private void HoverButton(Image image, bool isHovering)
        {
            image.color = isHovering ? JoystickController.HoverColor : JoystickController.NormalColor;
        }
    }
}